FROM zilleanai/zillean_backend
USER root
RUN apt update && apt install -y curl graphviz
RUN curl -sSL https://get.docker.com/ | sh
RUN usermod -aG docker flask
RUN pip install -U --no-cache-dir -e git+https://github.com/briancappello/flask-unchained.git#egg=flask-unchained
COPY zillean-domain.yml zillean-domain.yml
RUN zillean-cli domain install_requirements zillean-domain.yml --no_js
RUN rm -rf ./src/flask-unchained && pip install -U --no-cache-dir -e git+https://github.com/zilleanai/flask-unchained#egg=flask-unchained
USER flask
COPY unchained_config.py unchained_config.py
COPY routes.py backend/routes.py
COPY config.py backend/config.py
COPY bundles bundles
COPY docker/data data


