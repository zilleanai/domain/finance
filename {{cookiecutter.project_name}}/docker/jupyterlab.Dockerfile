# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

FROM jupyter/datascience-notebook

# Install jupyterlab
ARG JUPYTERLAB_VERSION=0.35.6
RUN conda install -c conda-forge jupyterlab --yes
RUN conda install -c conda-forge pandas XlsxWriter --yes
RUN pip install torch torchvision
RUN jupyter serverextension enable --py jupyterlab --sys-prefix
RUN pip install jupyterlab==$JUPYTERLAB_VERSION \
    &&  jupyter labextension install @jupyterlab/hub-extension

USER jovyan
COPY pgm.ipynb /home/jovyan/work/pgm.ipynb
COPY start.ipynb /home/jovyan/work/start.ipynb
COPY honchar /home/jovyan/work/honchar
