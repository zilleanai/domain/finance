import os
import pylab as plt
from zworkflow.train import TrainBase
import numpy as np
import networkx as nx
from pgmpy.estimators import HillClimbSearch, BicScore, BdeuScore

class train(TrainBase):

    def __init__(self, config):
        super().__init__(config)

    def train(self, dataset, model, logger=print):
        logger('Training started')
        df = dataset.data
        data = df[list(self.config['dataset']['features'])]
        data = (data*10)[:1500].dropna().astype(int)
        est = HillClimbSearch(data, BdeuScore(data))
        best_model = est.estimate(max_indegree=2, epsilon=1e-10, max_iter=1e8)
        sorted(best_model.nodes())
        nx.draw(best_model, with_labels=True)
        plt.savefig('graph.png')
        logger('Training finished')

    def __str__(self):
        return 'hillclimb trainer'
