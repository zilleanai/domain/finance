import os
import numpy as np
import pandas as pd
from zworkflow.dataset import DataSetBase


class dataset(DataSetBase):

    def __init__(self, config, preprocessing=None, data=None):
        super().__init__(config)
        self.features = config['dataset']['features']
        self.labels = config['dataset']['labels']
        self.preprocessing = preprocessing
        self.load(config['dataset']['datapath'], data)

    def load(self, datapath='.', data=None):
        tables = []
        if data:
            if type(data) is bytes:
                data = io.BytesIO(data)
                data.seek(0)
                df = pd.read_csv(data)
                df = self.prepare(df)
                tables.append(df)
            elif type(data) is list:
                self.files = data
                for f in self.files:
                    df = pd.read_csv(f)
                    df = self.prepare(df)
                    tables.append(df)
        else:
            self.files = sorted([f for f in os.listdir(datapath)
                                 if f.endswith('.csv') or f.endswith('.gz')])
            for f in self.files:
                df = pd.read_csv(os.path.join(datapath, f))
                df = self.prepare(df)
                tables.append(df)
        self.data = pd.concat(tables, axis=0, ignore_index=True)
        self.data = self.data.reindex()

    def __getitem__(self, idx):
        return self.data[self.features].loc[idx].values, self.data[self.labels].loc[idx].astype(np.float32).values

    def __len__(self):
        return len(self.data)

    def __str__(self):
        return "features: " + str(self.features) + " labels: " + str(self.labels) + " rows: " + str(len(self.data)) + str(self.files)
