# source: https://xgboost.readthedocs.io/en/latest/python/python_intro.html#
from zworkflow.model import ModelBase
import os

class model(ModelBase):

    def __init__(self, config):
        super().__init__(config)
        self.features = config['dataset']['features']
        self.labels = config['dataset']['labels']
        self.__model = None

    def model(self):
        return self.__model

    def set_model(self, model):
        self.__model = model

    def __str__(self):
        return 'hillclimb model: ' + str(self.features) + ', ' + str(self.labels)

    def save(self):
        pass

    def load(self):
        pass
