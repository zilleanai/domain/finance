# source: https://xgboost.readthedocs.io/en/latest/python/python_intro.html#
from zworkflow.model import ModelBase
import os
import json
from pgmpy.models import BayesianModel


class model(ModelBase):

    def __init__(self, config):
        super().__init__(config)
        self.__model = BayesianModel()

    def model(self):
        return self.__model

    def set_model(self, model):
        self.__model = model

    def __str__(self):
        return 'hillclimb model'

    def save(self):
        file_name = self.config['model']['pgm_id'] + '_pgm.json'
        json_ = self.pgmpy2json(self.__model, file_name)
        with open(file_name, 'w') as outfile:
            json.dump(json_, outfile)

    def load(self):
        file_name = self.config['model']['pgm_id'] + '_pgm.json'
        if os.path.exists(file_name):
            self.__model = self.pgmpy(file_name, BayesianModel)
            if self.config['general']['verbose']:
                print('loaded model: ', file_name)
        else:
            empty_model = {
                "id": str(id),
                "offsetX": 0,
                "offsetY": 0,
                "zoom": 100,
                "gridSize": 0,
                "links": [],
                "nodes": []
            }
            with open(file_name, 'w') as outfile:
                json.dump(empty_model, outfile)
            self.__model= self.pgmpy(file_name, BayesianModel)

    def pgmpy(self, file_name: str, Model):
        with open(file_name) as json_file:
            json_= json.load(json_file)
        return self.json2pgmpy(json_, Model)

    def json2pgmpy(self, json_data, Model):

        model= Model()
        nodes= {}
        for node in json_data.get('nodes') or []:
            model.add_node(node['name'].split(' ')[0])
            nodes[node['id']]= node['name']

        for link in json_data.get('links') or []:
            source= nodes[link['source']].split(' ')[0]
            target= nodes[link['target']].split(' ')[0]
            model.add_edge(source, target)
        return model

    def ports2json(self, node, edges):
        inports= []
        outports= []
        for edge in edges:
            if(edge[0] == node):
                outports.append(str(edge))
            if(edge[1] == node):
                inports.append(str(edge))

        return [
            {
                "id": node+'_in',
                "type": "default",
                "selected": False,
                "name": node+'_in',
                "parentNode": node,
                "links": inports,
                "in": True,
                "label": "X"
            },
            {
                "id": node+'_out',
                "type": "default",
                "selected": False,
                "name": node+'_out',
                "parentNode": node,
                "links": outports,
                "in": False,
                "label": "O"
            }
        ]

    def node2json(self, counter, node, edges):
        return {'id': node, 'name': node, 'type': 'default', "selected": False,
                "x": counter*50, "y": counter*50, "extras": {},
                "color": "rgb(0,192,255)",
                "ports": self.ports2json(node, edges)}

    def edge2json(self, counter, edge):
        return {'id': str(edge), 'type': 'default',
                "selected": False, "extras": {},
                "labels": [],
                "width": 3,
                "color": "rgba(255,255,255,0.5)",
                "curvyness": 50,
                "points": [
                    {
                        "id": "edf61659-1c40-4bd9-9799-d1bb8f8a920a",
                        "selected": False,
                        "x": 0,
                        "y": 0
                    },
                    {
                        "id": "7739e176-3cee-4d8e-a633-c8f7d89c5ec7",
                        "selected": True,
                        "x": 0,
                        "y": 0
                    }
        ],
            'source': edge[0],
            "sourcePort": edge[0]+"_out",
            'target': edge[1],
            "targetPort": edge[1]+"_in"}

    def pgmpy2json(self, model, file_name: str):
        with open(file_name) as json_file:
            json_= json.load(json_file)

        json_nodes= []
        for i, node in enumerate(model.nodes()):
            json_nodes.append(self.node2json(i, node, model.edges()))
        json_['nodes']= json_nodes

        json_edges= []
        for i, edge in enumerate(model.edges()):
            json_edges.append(self.edge2json(i, edge))
        json_['links']= json_edges
        return json_
