# source: https://github.com/yunjey/pytorch-tutorial/blob/master/tutorials/01-basics/feedforward_neural_network/main.py#L37-L49
import os
import io
from tqdm import tqdm
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torch.autograd import Variable

from zworkflow.evaluate import EvaluateBase


class evaluate(EvaluateBase):

    def __init__(self, config):
        super().__init__(config)
        self.device = torch.device(self.config['train']['device'])

    def evaluate(self, dataset, model):
        model.load()
        m = model.model()
        m.to(self.device)

        dataloader = DataLoader(dataset, batch_size=self.config['train']['batch_size'],
                                shuffle=False, num_workers=4)
        y_test = []
        X_test = []
        preds_test = []
        predicted = []
        for _, (X, y) in tqdm(enumerate(dataloader)):
            y = y.cpu().numpy().squeeze()
            y_test.extend(y)

            X_test.append(X)
            X = X.to(self.device)
            pred = model.model().predict(X).cpu().numpy().flatten()
            predicted.extend(pred)
        with torch.no_grad():
            for i in tqdm(range(100)):
                preds = []
                for X in X_test:
                    X = X.to(self.device)
                    pred = model.model().predict(X).cpu().numpy().flatten()
                    preds.extend(pred)
                preds_test.append(np.array(preds))

        y_test = np.array(y_test)
        preds_test = np.array(preds_test)
        predicted = np.array(predicted)
        mean = np.mean(preds_test, axis=0)
        std = np.std(preds_test, axis=0) / 10

        x = np.arange(len(y_test))

        plt.figure()
        plt.plot(x, y_test)
        plt.plot(x, mean, linestyle='--')
        plt.fill_between(x, mean-std, mean+std, alpha=0.3, color='orange')
        
        plt.savefig('honchar.png')
        plt.show()
        return { 'images': ['honchar.png'] }

    def __str__(self):
        return 'honchar predict'
