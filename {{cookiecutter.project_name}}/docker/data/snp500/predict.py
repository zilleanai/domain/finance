# source: https://github.com/yunjey/pytorch-tutorial/blob/master/tutorials/01-basics/feedforward_neural_network/main.py#L37-L49
import os
import io
from tqdm import tqdm
import numpy as np
import pandas as pd

from zworkflow.predict import PredictBase


class predict(PredictBase):

    def __init__(self, config):
        super().__init__(config)

    def predict(self, dataset, model):

        model.load()
        pgm = model.model()

        data = dataset.data
        features = list(set(data.columns.tolist()) & set(self.config['dataset']['features']))
        data = data[features]
        data = (data*100).dropna().astype(int)

        batch_size = self.config['train']['batch_size']
        total_step = len(data) // batch_size
        predicted = []
        for i in tqdm(range(total_step)):
            X = data[i*batch_size:(i+1)*batch_size]
            pred = pgm.predict(X)
            predicted.append(pred)
        if(total_step == 0):
            df = pgm.predict(data)
        else:
            df = pd.concat(predicted, axis=0, ignore_index=True)
        df = df / 100.0
        csv = df.to_csv(index=False)
        return csv.encode()

    def __str__(self):
        return 'predict'
