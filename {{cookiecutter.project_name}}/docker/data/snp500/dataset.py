import io
import os
import functools 
import numpy as np
import pandas as pd
from zworkflow.dataset import DataSetBase


class dataset(DataSetBase):

    def __init__(self, config, preprocessing=None, data=None):
        super().__init__(config)
        self.features = config['dataset']['features']
        self.preprocessing = preprocessing
        self.load(config['dataset']['datapath'], data)

    def prepare(self, df):
        float_cols = [c for c in df if df[c].dtype == np.float64]
        df[float_cols] = df[float_cols].astype(np.float32)
        if self.preprocessing:
            df = self.preprocessing.process(df)
        df = df.dropna()
        return df

    def load(self, datapath='.', data=None):
        tables = []
        if data:
            if type(data) is bytes:
                data = io.BytesIO(data)
                data.seek(0)
                df = pd.read_csv(data)
                df = self.prepare(df)
                tables.append(df)
            elif type(data) is list:
                self.files = data
                for f in self.files:
                    df = pd.read_csv(f)
                    df = self.prepare(df)
                    tables.append(df)
        else:
            self.files = sorted([(f,f+'_data.csv') for f in self.features])
            for (name, f) in self.files:
                df = pd.read_csv(os.path.join(datapath, f))
                df[name] = df['close']
                df = df[['date', name]]
                tables.append(df)
        self.data = functools.reduce(lambda x, y: pd.merge(x, y, on = 'date'), tables)

        self.data = self.data.reindex()
        #self.data.to_csv("test_gt.csv")

    def __getitem__(self, idx):
        return self.data[self.features].loc[idx].values

    def __len__(self):
        return len(self.data)

    def __str__(self):
        return "features: " + str(self.features) + " rows: " + str(len(self.data)) + str(self.files)
