import os
import time
from tqdm import tqdm
import matplotlib.pyplot as plt
from zworkflow.train import TrainBase
import numpy as np
import networkx as nx
from pgmpy.estimators import MaximumLikelihoodEstimator, BayesianEstimator


class train(TrainBase):

    def __init__(self, config):
        super().__init__(config)

    def save(self, model, best_model):
        sorted(best_model.nodes())
        model.set_model(best_model)
        model.save()
        nx.draw(best_model, with_labels=True)
        plt.savefig('graph.png')

    def train(self, dataset, model, logger=print):
        model.load()
        start = time.time()
        logger('Training started')
        df = dataset.data
        data = df[list(self.config['dataset']['features'])]
        data = (data*100).dropna().astype(int)
        if self.config['train'].get('last_data'):
            data = data.tail(self.config['train']['last_data'])
            print("data size: ", len(data))

        pgm = model.model()
        batch_size = self.config['train']['batch_size']
        total_step = 1 + len(data) // batch_size

        for i in tqdm(range(total_step)):
            X = data[i*batch_size:(i+1)*batch_size]
            pgm.fit(X, estimator=BayesianEstimator)
            model.set_model(pgm)
            if i % self.config['train']['save_every_epoch'] == 0:
                model.save()
            break
        model.save()

        end = time.time()
        logger('Training finished, time taken: ', end-start)

    def __str__(self):
        return 'hillclimb trainer'
