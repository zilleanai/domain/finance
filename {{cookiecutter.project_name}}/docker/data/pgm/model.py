# source: https://medium.com/@alexrachnog/financial-forecasting-with-probabilistic-programming-and-pyro-db68ab1a1dba
# source: http://pyro.ai/examples/bayesian_regression.html
# source: https://github.com/pyro-ppl/pyro/blob/dev/examples/vae/ss_vae_M2.py
# source: https://github.com/pyro-ppl/pyro/blob/dev/examples/vae/utils/custom_mlp.py
from zworkflow.model import ModelBase
import json
import os
from inspect import isclass
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.nn import Softplus
import pyro
import pyro.contrib.gp as gp
from pyro.distributions.util import broadcast_shape
from pyro.distributions import Beta, Cauchy, Normal, Poisson, Uniform, Delta, Bernoulli
from pyro.contrib.autoguide import AutoDiagonalNormal
import pyro.distributions as dist
pyro.get_param_store().clear()


class RegressionModel(nn.Module):
    # source: https://medium.com/@alexrachnog/financial-forecasting-with-probabilistic-programming-and-pyro-db68ab1a1dba
    def __init__(self, p):
        super(RegressionModel, self).__init__()
        self.linear = nn.Linear(p, 1)

    def forward(self, x):
        # x * w + b
        return self.linear(x)


class Net(torch.nn.Module):
    # source: https://medium.com/@alexrachnog/financial-forecasting-with-probabilistic-programming-and-pyro-db68ab1a1dba
    def __init__(self, n_feature, n_hidden):
        super(Net, self).__init__()
        self.hidden = torch.nn.Linear(n_feature, n_hidden)   # hidden layer
        self.predict = torch.nn.Linear(n_hidden, 1)   # output layer

    def forward(self, x):
        x = self.hidden(x)
        x = self.predict(x)
        return x


class PPModel(nn.Module):
    def __init__(self, p=1, n_hidden=25, N=360, dists={}):
        super(PPModel, self).__init__()
        self.p = p
        self.N = N
        self.softplus = nn.Softplus()
        self.sigmoid = nn.Sigmoid()
        self.first_layer = p
        self.second_layer = n_hidden
        self.dists = dists
        self.regression_model = Net(self.p, n_hidden)

    def model(self, X, y):
        mu = Variable(torch.zeros(self.second_layer,
                                  self.first_layer)).type_as(X)
        sigma = Variable(torch.ones(self.second_layer,
                                    self.first_layer)).type_as(X)
        bias_mu = Variable(torch.zeros(self.second_layer)).type_as(X)
        bias_sigma = Variable(torch.ones(self.second_layer)).type_as(X)
        w_prior, b_prior = Normal(mu, sigma), Normal(bias_mu, bias_sigma)

        mu2 = Variable(torch.zeros(1, self.second_layer)).type_as(X)
        sigma2 = Variable(torch.ones(1, self.second_layer)).type_as(X)
        bias_mu2 = Variable(torch.zeros(1)).type_as(X)
        bias_sigma2 = Variable(torch.ones(1)).type_as(X)
        w_prior2, b_prior2 = Normal(mu2, sigma2), Normal(bias_mu2, bias_sigma2)

        priors = {'hidden.weight': w_prior,
                  'hidden.bias': b_prior,
                  'predict.weight': w_prior2,
                  'predict.bias': b_prior2}

        # lift module parameters to random variables sampled from the priors
        lifted_module = pyro.random_module(
            "module", self.regression_model, priors)
        # sample a regressor (which also samples w and b)
        lifted_reg_model = lifted_module()

        with pyro.iarange("map", self.N, subsample=X):
            x_data = X
            y_data = y
            # run the regressor forward conditioned on inputs
            prediction_mean = lifted_reg_model(x_data).squeeze()
            pyro.sample("obs",
                        Normal(prediction_mean, Variable(
                            torch.ones(X.size(0))).type_as(X)),
                        obs=y_data.squeeze())

    def guide(self, X, y=None):
        w_mu = Variable(torch.randn(
            self.second_layer, self.first_layer).type_as(X), requires_grad=True)
        w_log_sig = Variable((torch.ones(self.second_layer, self.first_layer) + 0.05 * torch.randn(
            self.second_layer, self.first_layer)).type_as(X), requires_grad=True)
        b_mu = Variable(torch.randn(self.second_layer).type_as(
            X), requires_grad=True)
        b_log_sig = Variable((torch.ones(self.second_layer) + 0.05 *
                              torch.randn(self.second_layer)).type_as(X), requires_grad=True)

        # register learnable params in the param store
        mw_param = pyro.param("guide_mean_weight", w_mu)
        sw_param = self.softplus(pyro.param(
            "guide_log_sigma_weight", w_log_sig))
        mb_param = pyro.param("guide_mean_bias", b_mu)
        sb_param = self.softplus(pyro.param("guide_log_sigma_bias", b_log_sig))

        # gaussian guide distributions for w and b
        w_dist = Normal(mw_param, sw_param)
        b_dist = Normal(mb_param, sb_param)

    #     w_mu2 = Variable(torch.randn(1, second_layer).type_as(data.data), requires_grad=True)
    #     w_log_sig2 = Variable(torch.randn(1, second_layer).type_as(data.data), requires_grad=True)
    #     b_mu2 = Variable(torch.randn(1).type_as(data.data), requires_grad=True)
    #     b_log_sig2 = Variable(torch.randn(1).type_as(data.data), requires_grad=True)

        w_mu2 = Variable(torch.randn(1, self.second_layer).type_as(
            X), requires_grad=True)
        w_log_sig2 = Variable((torch.ones(1, self.second_layer) + 0.05 *
                               torch.randn(1, self.second_layer)).type_as(X), requires_grad=True)
        b_mu2 = Variable(torch.randn(1).type_as(X), requires_grad=True)
        b_log_sig2 = Variable(
            (torch.ones(1) + 0.05 * torch.randn(1)).type_as(X), requires_grad=True)

        # register learnable params in the param store
        mw_param2 = pyro.param("guide_mean_weight2", w_mu2)
        sw_param2 = self.softplus(pyro.param(
            "guide_log_sigma_weight2", w_log_sig2))
        mb_param2 = pyro.param("guide_mean_bias2", b_mu2)
        sb_param2 = self.softplus(pyro.param(
            "guide_log_sigma_bias2", b_log_sig2))

        # gaussian guide distributions for w and b
        w_dist2 = Normal(mw_param2, sw_param2)
        b_dist2 = Normal(mb_param2, sb_param2)

        dists = {'hidden.weight': w_dist,
                 'hidden.bias': b_dist,
                 'predict.weight': w_dist2,
                 'predict.bias': b_dist2}

        # overloading the parameters in the module with random samples from the guide distributions
        lifted_module = pyro.random_module(
            "module", self.regression_model, dists)
        # sample a regressor
        return lifted_module()

    def predict(self, xs):
        sampled_reg_model = self.guide(xs)
        ys = sampled_reg_model(xs).data
        return ys


def pyro_distribution(distribution):
    name = distribution['distname']
    param1 = distribution['param1']
    param2 = distribution['param2']
    param3 = distribution['param3']
    if name == 'Beta':
        return {'dist': Beta(concentration1=param2, concentration2=param3), 'hidden': param1}
    if name == 'Binomial':
        return {'dist': Bernoulli(probs=param3), 'hidden': param1}
    if name == 'Cauchy':
        return {'dist': Cauchy(loc=param2, scale=param3), 'hidden': param1}
    if name == 'Normal':
        return {'dist': Normal(loc=param2, scale=param3), 'hidden': param1}
    if name == 'Uniform':
        return {'dist': Uniform(low=param2, high=param3), 'hidden': param1}
    if name == 'Poisson':
        return {'dist': Poisson(rate=param2), 'hidden': param1}
    return {'dist': Normal(loc=0, scale=1), 'hidden': param1}


class model(ModelBase):

    def __init__(self, config):
        super().__init__(config)
        self.features = config['dataset']['features']
        self.labels = config['dataset']['labels']
        self.window = config['dataset']['window']
        self.n_hidden = config['model']['n_hidden']
        self.pgm_id = config['model']['pgm_id']
        self.bs = config['train']['batch_size']
        self.N = 360
        self.dists = self.getDistributions()
        self.initModel()


    def initModel(self):
        N = self.N
        pyro.get_param_store().clear()
        self.__model = PPModel(p=len(self.features)*self.window,
                               n_hidden=self.n_hidden, N=N, dists=self.dists)

    def getDistributions(self):
        filename = str(self.pgm_id) + '_distributions.json'
        if os.path.exists(filename):
            with open(filename, 'r') as infile:
                distributions = json.load(infile)
        else:
            return []
        pyro_distributions = {}
        for distribution in distributions:
            pyro_distributions[distribution] = pyro_distribution(
                distributions[distribution])
        return pyro_distributions

    def model(self):
        return self.__model

    def __str__(self):
        return str(self.features) + ', ' + str(self.labels)

    def save(self):
        pyro.get_param_store().save(self.config['model']['savepath'])
        if self.config['general']['verbose']:
            print('saved model: ', self.config['model']['savepath'])

    def load(self):
        if os.path.exists(self.config['model']['savepath']):
            pyro.get_param_store().load(self.config['model']['savepath'])
            if self.config['general']['verbose']:
                print('loaded model: ', self.config['model']['savepath'])
