import React from 'react'
import { Route, Switch } from 'react-router-dom'
import startCase from 'lodash/startCase'
import { compile } from 'path-to-regexp'

import {
  ForgotPassword,
  Login,
  Logout,
  PendingConfirmation,
  Profile,
  SignUp,
  ResendConfirmation,
  ResetPassword,
} from 'security/pages'

import {
  Contact,
  Home,
  NotFound,
  Styles,
} from 'site/pages'

import {
  Projects
} from 'comps/project/pages'
import {
  Git,
} from 'comps/git/pages'
import {
  TagDetail,
  Tags
} from 'comps/tag/pages'

import {
  Files
} from 'comps/file/pages'

import {
  Csv,
  LabelCsv,
  Csvs
} from 'comps/csv/pages'

import {
  Modeler,
  PGMs
} from 'comps/pgm/pages'

import {
  DownloadRecord,
} from 'comps/cryptocoindata/pages'
import {
  Workflow,
} from 'comps/workflow/pages'
import {
  Train,
} from 'comps/train/pages'
import {
  Labeling,
} from 'comps/labeling/pages'
import {
  Preprocessing,
} from 'comps/preprocessing/pages'
import {
  Predict,
} from 'comps/predict/pages'
import {
  PredictCsv,
} from 'comps/predict_csv/pages'
import {
  Evaluate,
} from 'comps/evaluate/pages'
import {
  Backtest,
} from 'comps/backtest/pages'
import {
  DockerRunner,
} from 'comps/docker_runner/pages'
import {
  DocDetail,
  Docs
} from 'comps/docs/pages'

import { AnonymousRoute, ProtectedRoute } from 'utils/route'


/**
 * ROUTES: The canonical store of frontend routes. Routes throughout the system
 * should be referenced using these constants
 *
 * Both keys and values are component class names
 */
export const ROUTES = {
  Contact: 'Contact',
  ForgotPassword: 'ForgotPassword',
  Home: 'Home',
  Login: 'Login',
  Logout: 'Logout',
  PendingConfirmation: 'PendingConfirmation',
  Profile: 'Profile',
  ResendConfirmation: 'ResendConfirmation',
  ResetPassword: 'ResetPassword',
  SignUp: 'SignUp',
  Styles: 'Styles',
  Projects: 'Projects',
  Git: 'Git',
  TagDetail: 'TagDetail',
  Tags: 'Tags',
  Files: 'Files',
  Csv: 'Csv',
  LabelCsv: 'LabelCsv',
  Csvs: 'Csvs',
  Modeler: 'Modeler',
  PGMs: 'PGMs',
  DownloadRecord: 'DownloadRecord',
  Train: 'Train',
  Workflow: 'Workflow',
  Labeling: 'Labeling',
  Preprocessing: 'Preprocessing',
  Predict: 'Predict',
  PredictCsv: 'PredictCsv',
  Evaluate: 'Evaluate',
  Backtest: 'Backtest',
  DockerRunner: 'DockerRunner',
  DocDetail: 'DocDetail',
  Docs: 'Docs',
}

/**
 * route details
 *
 * list of objects with keys:
 *  - key: component class name
 *  - path: the path for the component (in react router notation)
 *  - component: The component to use
 *  - routeComponent: optional, AnonymousRoute, ProtectedRoute or Route (default: Route)
 *  - label: optional, label to use for links (default: startCase(key))
 */
const routes = [
  {
    key: ROUTES.Contact,
    path: '/contact',
    component: Contact,
  },
  {
    key: ROUTES.ForgotPassword,
    path: '/login/forgot-password',
    component: ForgotPassword,
    routeComponent: AnonymousRoute,
    label: 'Forgot password?',
  },
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  {
    key: ROUTES.Login,
    path: '/login',
    component: Login,
    routeComponent: AnonymousRoute,
    label: 'Login',
  },
  {
    key: ROUTES.Logout,
    path: '/logout',
    component: Logout,
    label: 'Logout',
  },
  {
    key: ROUTES.PendingConfirmation,
    path: '/sign-up/pending-confirm-email',
    component: PendingConfirmation,
    routeComponent: AnonymousRoute,
    label: 'Pending Confirm Email',
  },
  {
    key: ROUTES.Profile,
    path: '/profile',
    component: Profile,
    routeComponent: ProtectedRoute,
    label: 'Profile',
  },
  {
    key: ROUTES.ResendConfirmation,
    path: '/sign-up/resend-confirmation-email',
    component: ResendConfirmation,
    routeComponent: AnonymousRoute,
    label: 'Resend Confirmation Email',
  },
  {
    key: ROUTES.ResetPassword,
    path: '/login/reset-password/:token',
    component: ResetPassword,
    routeComponent: AnonymousRoute,
    label: 'Reset Password',
  },
  {
    key: ROUTES.SignUp,
    path: '/sign-up',
    component: SignUp,
    routeComponent: AnonymousRoute,
    label: 'Sign Up',
  },
  {
    key: ROUTES.Styles,
    path: '/styles',
    component: Styles,
  },
  {
    key: ROUTES.Projects,
    path: '/projects',
    component: Projects,
  },
  {
    key: ROUTES.Git,
    path: '/git',
    component: Git,
  },
  {
    key: ROUTES.TagDetail,
    path: '/tags/:name',
    component: TagDetail,
  },
  {
    key: ROUTES.Tags,
    path: '/tags',
    component: Tags,
  },
  {
    key: ROUTES.Files,
    path: '/files/:subpath',
    component: Files,
  },
  {
    key: ROUTES.Csv,
    path: '/csv/:id',
    component: Csv,
  },
  {
    key: ROUTES.LabelCsv,
    path: '/labelcsv/:id',
    component: LabelCsv,
  },
  {
    key: ROUTES.Csvs,
    path: '/csvs/:subpath',
    component: Csvs,
  },
  {
    key: ROUTES.Modeler,
    path: '/modeler/:id',
    component: Modeler,
  },
  {
    key: ROUTES.PGMs,
    path: '/pgms',
    component: PGMs,
  },
  {
    key: ROUTES.DownloadRecord,
    path: '/downloadrecord',
    component: DownloadRecord,
  },
  {
    key: ROUTES.Workflow,
    path: '/workflow',
    component: Workflow,
  },
  {
    key: ROUTES.Train,
    path: '/train',
    component: Train,
  },
  {
    key: ROUTES.Labeling,
    path: '/labeling',
    component: Labeling,
  },
  {
    key: ROUTES.Preprocessing,
    path: '/preprocessing',
    component: Preprocessing,
  },
  {
    key: ROUTES.Predict,
    path: '/predict',
    component: Predict,
  },
  {
    key: ROUTES.PredictCsv,
    path: '/predict_csv',
    component: PredictCsv,
  },
  {
    key: ROUTES.Evaluate,
    path: '/evaluate',
    component: Evaluate,
  },
  {
    key: ROUTES.Backtest,
    path: '/backtest',
    component: Backtest,
  },
  {
    key: ROUTES.DockerRunner,
    path: '/docker_runner',
    component: DockerRunner,
  },
  {
    key: ROUTES.DocDetail,
    path: '/docs/:name',
    component: DocDetail,
  },
  {
    key: ROUTES.Docs,
    path: '/docs',
    component: Docs,
  },
]

/**
 * ROUTE_MAP: A public lookup for route details by key
 */
export const ROUTE_MAP = {}
routes.forEach((route) => {
  let { component, key, label, path, routeComponent } = route

  if (!component) {
    throw new Error(`component was not specified for the ${key} route!`)
  }
  if (!path) {
    throw new Error(`path was not specified for the ${key} route!`)
  }

  ROUTE_MAP[key] = {
    path,
    toPath: compile(path),
    component,
    routeComponent: routeComponent || Route,
    label: label || startCase(key),
  }
})

/**
 * React Router 4 re-renders all child components of Switch statements on
 * every page change. Therefore, we render routes ahead of time once.
 */
const cachedRoutes = routes.map((route) => {
  const { component, path, routeComponent: RouteComponent } = ROUTE_MAP[route.key]
  return <RouteComponent exact path={path} component={component} key={path} />
})
cachedRoutes.push(<Route component={NotFound} key="*" />)

export default () => (
  <Switch>
    {cachedRoutes}
  </Switch>
)

