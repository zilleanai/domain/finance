# Deep Neural Network

## Projects

First create a project.

![Create New Project](res/create_new_project.png)
![Project Created](res/project_created.png)
Select created project.

![Project Changed](res/project_changed.png)

## Tags

Create a tag for files called 'file'.

![Create Tag](res/create_tag.png)

## Records

Download record data into the platform.

![Download Record](res/download_record.png)

## CSVs

View downloaded data.

![CSVS](res/csvs.png)

## Workflow files

Upload workflow files to the platform.

```python
# model.py
class NeuralNet(nn.Module):
    def __init__(self, input_size, output_size):
        super(NeuralNet, self).__init__()
        self.fc1 = nn.Linear(input_size, input_size*5)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(input_size*5, input_size*3)
        self.fc3 = nn.Linear(input_size*3, input_size)
        self.fc4 = nn.Linear(input_size, output_size)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        out = self.fc1(x)
        out = self.relu(out)
        out = self.fc2(out)
        out = self.relu(out)
        out = self.fc3(out)
        out = self.relu(out)
        out = self.fc4(out)
        out = self.sigmoid(out)
        return out
```

![Upload Workflow Files](res/upload_workflow_files.png)

## Workflow Configuration

Change workflow configuration.

```yaml
# workflow.yml
dataset:
  dataset_file: dataset.py
  datapath: .
  features:
    - price
    - ask_price_0
    - ask_price_1
    - bid_price_0
    - bid_price_1
    - ask_quantity_0
    - ask_quantity_1
    - bid_quantity_0
    - bid_quantity_1
    - high_1m
    - low_1m
    - high_1h
    - low_1h
    - moving_average10
    - moving_average50
    - exponential_moving_average10
    - exponential_moving_average50
    - macd
    - volume_moving_average10
  labels:
    - action
preprocessing:
  preprocessing_file: preprocessing.py
  functions:
    - moving_average10
    - moving_average50
    - exponential_moving_average10
    - exponential_moving_average50
    - macd
    - volume_moving_average10
model:
  model_file: model.py
  savepath: model.save
train:
  train_file: train.py
  shuffle: true
  learn_rate: 0.0001
  epochs: 2
  batch_size: 100
  load_model: true
  device: "cpu"
predict:
  predict_file: predict.py
```

![Change Workflow Config](res/change_workflow_config.png)

## Labeling

Label training data.

```python
# label.py
class label(LabelBase):

    def __init__(self, config):
        super().__init__(config)

    def label(self, verbose=False, logger=print):
        datapath = self.config['dataset']['datapath']
        files = sorted([f for f in os.listdir(datapath)
                        if f.endswith('.csv') or f.endswith('.gz')])

        for f in files:
            logger('label file: ', f)
            df = pd.read_csv(os.path.join(datapath, f))
            df['action'] = np.log(df['price'] / df['price'].shift(10))
            df.to_csv(os.path.join(datapath, f), compression='gzip', index=False)

```

![Label Data](res/label_data.png)

## Preprocessing

Select preprocess functions.

![Preprocessing](res/preprocess.png)

## Training

Start training.

![Training](res/train.png)

## Prediction

Upload a csv file and make a prediction.

![Predict](res/predict.png)
