# honchar - Financial forecasting with probabilistic programming and Pyro

[Source: Medium Article by Alexandr Honchar](https://medium.com/@alexrachnog/financial-forecasting-with-probabilistic-programming-and-pyro-db68ab1a1dba)

## Projects

First create a new project called honchar.

![Create New Project](res/honchar/create_project.png)

Then select the created project.

![Project Created](res/honchar/select_project.png)

## Tags

We need a tag to upload files.
Create a tag for files called 'file'.

![Create File Tag](res/honchar/create_file_tag.png)

## Files

Now we crete a new folder for the data called 'data'.

![Create Data Dir](res/honchar/create_data_dir.png)

We select into this data folder and upload a data file.

![Upload data](res/honchar/upload_data.png)

## CSVs

On the csv page we can view the uploaded data file.

![CSVS](res/honchar/csvs.png)

We now export columns to tags. They will be used later.

![Export CSV Tags](res/honchar/export_csv_tags.png)

## Workflow files

Upload workflow files to the platform.

![Upload Workflow Files](res/honchar/upload_workflow_files.png)

## Workflow Configuration

Change workflow configuration.

Change used features in the model or window size.
Labels are calculated by a sliding window on the time series data.

![Change Workflow Config](res/honchar/change_workflow_config.png)

## Labeling

Start labeling training data on the Labeling page.

![Start Labeling](res/honchar/start_labeling.png)

## Preprocessing

Configure preprocessing step by selecting preprocess functions.

![Preprocessing](res/honchar/config_preprocess.png)

## Training

Start training on the Train page.

![Training](res/honchar/start_training.png)

## Prediction

Upload a csv file and make a prediction.
The results can be viewed on a plot and compared to the input features.

![Predict](res/honchar/predict_csv.png)

## Evaluate

Upload a labeled csv file and make an evaluation.
The results are visualized in an image.

![Evaluate](res/honchar/evaluate_csv.png)


## Backtest

Upload a csv file and make a backtest.
The results a visualized in a plot.

![Backtest](res/honchar/backtest_csv.png)
