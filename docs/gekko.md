# gekko

## Projects

First create a new project called gekko.

![Create New Project](res/gekko/create_project.png)

Then select the created project.

![Project Created](res/gekko/select_project.png)

## Tags

We need a tag to upload files.
Create a tag for files called 'file'.

![Create File Tag](res/gekko/create_file_tag.png)

## Download Data

Now we download data from a trading platform.

![Download Record](res/gekko/download_record.png)

## CSVs

On the csv page we can view the downloaded data files.

![CSVS](res/gekko/csvs.png)

We now export columns to tags. They will be used later.

![Export CSV Tags](res/gekko/export_csv_tags.png)

## Workflow files

Upload workflow files to the platform.

![Upload Workflow Files](res/gekko/upload_workflow_files.png)

## Workflow Configuration

Change workflow configuration.

Change used features in the model or window size.
Labels are calculated by a sliding window on the time series data.

![Change Workflow Config](res/gekko/change_workflow_config.png)

## Labeling

Start labeling training data on the Labeling page.

![Start Labeling](res/gekko/start_labeling.png)

## Preprocessing

Configure preprocessing step by selecting preprocess functions.

![Preprocessing](res/gekko/config_preprocess.png)

## Training

Start training on the Train page.

![Training](res/gekko/start_training.png)

## Prediction

Upload a csv file and make a prediction.
The results can be viewed on a plot and compared to the input features.

![Predict](res/gekko/predict_csv.png)

## Backtest

Upload a csv file and make a backtest.
The results a visualized in a plot.

![Backtest](res/gekko/backtest_csv.png)
