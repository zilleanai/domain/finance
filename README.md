# finance

Finance Domain

## Quickstart

```bash
git clone git+https://gitlab.com/zilleanai/domain/finance
```

## Installation

```bash
pip install git+https://github.com/zilleanai/zillean_cli
zillean-cli domain install https://gitlab.com/zilleanai/domain/finance
```

## Example

[Deep Neural Network](docs/DNN.md)

[Convolutional Neural Network](docs/CNN.md)
